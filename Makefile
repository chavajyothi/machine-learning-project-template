.PHONY: virtualenv conda install install-dev build clean

#################################################################################
## GLOBALS                                                                     ##
#################################################################################

SHELL   		= /bin/bash
PYTHON 			= python
PROJECT_DIR 	= $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME 	= $(shell basename $(CURDIR))

#################################################################################
## COMMANDS                                                                    ##
#################################################################################

## Create virtualenv.
## Activate with the command:
## source venv/bin/activate
virtualenv:
	virtualenv -p $(PYTHON) venv

## Create conda environment with python 3.8.
## Activate with the command:
## conda activate PROJECT_NAME
conda:
	conda create -n $(PROJECT_NAME) python=3.8
	echo "Activate with the command: conda activate $(PROJECT_NAME)"

## Install Python dependencies.
## Make sure you activate the environment first!
install:
	$(PYTHON) -m pip install -r requirements.txt

## Install Python dependencies for developers.
## Make sure you activate the environment first!
install-dev:
	$(PYTHON) -m pip install -r requirements.txt -r requirements-dev.txt

## Create user settings file.
## Create directories for data, results and reports.
build:
	touch src/user_settings.py
	@mkdir -vp $(PROJECT_DIR)/data/{raw,samples,processed,datasets}
	@mkdir -vp $(PROJECT_DIR)/results/{checkpoints,logs/{tensorboard,base_logs},models}
	@mkdir -vp $(PROJECT_DIR)/reports

## Delete all compiled Python files.
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete


#################################################################################
## Help                                                                        ##
#################################################################################
.PHONY: help

.DEFAULT_GOAL := help

help:
	@echo "Usage:"
	@echo "    virtualenv [PYTHON='']"
	@echo "    conda [PROJECT_NAME='']"
	@echo "    install [PYTHON='']"
	@echo "    install-dev [PYTHON='']"
	@echo "    build"
	@echo "    clean"
